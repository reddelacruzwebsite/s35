// Express Setup

const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 8000;

mongoose.connect(`mongodb+srv://reddelacruzwebsite:admin123@cluster0.itpeetc.mongodb.net/s35-activity?retryWrites=true&w=majority`, {

	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection"));
db.once('open', () => console.log('Connection to MongoDB!'))

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// SCHEMA
const userSchema = new mongoose.Schema({

	name: String,
	password: String
})

// MODEL
const User = mongoose.model('User', userSchema);


// ROUTES
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.post('/signup', (req, res) => {
	// business logic

	User.findOne({name: req.body.name}, (error, result) => {
		if(error){
			return res.send(error)
		} else if (result != null && result.name == req.body.name) {
			return res.send('Username already exists')
		} else {
			let newUser = new User({
				name: req.body.name,
				password: req.body.password
			})

			newUser.save((error, savedUser) => {
				if(error) {
					return console.error(error)
				} else res.status(201).send('New user registered!')
			})
		}
	})
})
app.listen(port, () => console.log(`Server is running at port: ${port}`))